#=
20220911 remove trivial intermediate module

compare lines in two versions of a file, and rate inexact lines.
=#

include("modules/FileLineVerify.jl")
using .FileLineVerify
include("modules/mod_Functions.jl")
using .Functions 

using Gtk

# UI values
username = splitdir(homedir())[end] # works on linux and windows?
startDir = "/home/$username/Dropbox/JOURNAL"
text_label_info = "Compare two files to check if the newer one has all the lines of the older one.\nCreate a file with lines that do not exaclty match, with a rating prefix."
button_setfile_old_text = "Set Older file"
button_setfile_new_text = "Set Newer file"
checkbox_month_text = " Compare against whole month not day"
button_start_text = "Start Compare"
window_title = "Compare 2 Files For Line Presence"
selected_file_old_dialog_text = "Compare 2 Files - SELECT OLDER FILE VERSION..."
selected_file_new_dialog_text = "Compare 2 Files - SELECT NEWER FILE VERSION..."
feedback_already_processing = " Already processing, please wait for result message."
feedback_select_both = " Select both files."
feedback_both_files = " Both must be files."
button_start_text_working = "Comparing..."
feedback_file_old_blank = " Older file is empty or unavailable, ending operation."
feedback_checking = " Checking if lines in older file are in newer file."
feedback_completed = " Check complete."

bool_compare_month = false
#selected_file_old = "" # needed when not diplaying whole path
#selected_file_new = ""

function start_comparison(array_lines_older::AbstractArray, array_lines_newer::AbstractArray, selected_file_old::AbstractString, bool_compare_month::Bool)
    #println("module function called, calling fileLinesVerify function.")
    return FileLineVerify.check_lines_arein_linearray(array_lines_older, array_lines_newer, selected_file_old, true, bool_compare_month)
end

function get_filepath_text_bydialog(title::AbstractString="", selectN::Bool=false)
    return open_dialog(title, GtkNullContainer(), ("*.txt, *.csv",), select_multiple=selectN)
end

function clear_feedback()
    GAccessor.label(textbox_output, "")
    GAccessor.label(textbox_progress, "")  
end

# CREATE THE GUI
textbox_info = GtkLabel(text_label_info)
set_gtk_property!(textbox_info, :xalign, 0.5)
GAccessor.line_wrap(textbox_info,true)

text_file_old = GtkEntry()
text_file_new = GtkEntry()
set_gtk_property!(text_file_old, :editable, false)
set_gtk_property!(text_file_new, :editable, false)
set_gtk_property!(text_file_old, :xalign, 0.05)
set_gtk_property!(text_file_new, :xalign, 0.05)
# during testing
#GAccessor.text(text_file_old, "/home/i/Dropbox/JOURNAL/test_older.txt")
#GAccessor.text(text_file_new, "/home/i/Dropbox/JOURNAL/test_newer.txt")


button_setfile_old = GtkButton(button_setfile_old_text)
button_setfile_new = GtkButton(button_setfile_new_text)

checkbox_month = GtkCheckButton(checkbox_month_text)
#set_gtk_property!(checkbox_month, :label, "Compare against all month entries")
button_start = GtkButton(button_start_text)
textbox_output = GtkLabel("")
set_gtk_property!(textbox_output,:xalign, 0.1)
set_gtk_property!(textbox_output,:yalign, 0.0)

GAccessor.selectable(textbox_output, true)
GAccessor.line_wrap(textbox_output, true)
#progressbar = GtkProgressBar()
#GAccessor.show_text(progressbar, true)
textbox_progress = GtkLabel("")
set_gtk_property!(textbox_progress,:xalign, 0.1)

# create the grid and arrange widgets in it
grid = GtkGrid()
set_gtk_property!(grid, :column_homogeneous, true)
grid[1:6,1] = textbox_info # span and set 6 columns
grid[1,2] = button_setfile_old 
grid[2:6,2] = text_file_old 
grid[1,3] = button_setfile_new 
grid[2:6,3] = text_file_new 

grid[1:3,4] = checkbox_month

grid[1,5] = button_start 
grid[2:6,5] = textbox_progress 
grid[1:6,6] = textbox_output 

#create the window and add the grid to it.
window = GtkWindow(window_title, 1000, 400)
GAccessor.resizable(window, true)
set_gtk_property!(window, :border_width, 5)
push!(window, grid)
showall(window)

button_setfile_old_triggered = signal_connect(button_setfile_old, "clicked") do widget
    selected_file_old = get_filepath_text_bydialog(selected_file_old_dialog_text, false)
    #filename_old = Functions.get_path_child(selected_file_old)
    #GAccessor.text(text_file_old, filename_old)
    GAccessor.text(text_file_old, selected_file_old)
    clear_feedback()
end
button_setfile_new_triggered = signal_connect(button_setfile_new, "clicked") do widget
    selected_file_new = get_filepath_text_bydialog(selected_file_new_dialog_text, false)
    #filename_new = Functions.get_path_child(selected_file_new)
    #GAccessor.text(text_file_new, filename_new)
    GAccessor.text(text_file_new, selected_file_new)
    clear_feedback()
end

checkbox_month_triggered = signal_connect(checkbox_month, "toggled") do widget
    global bool_compare_month = get_gtk_property(checkbox_month, :active, Bool)
    #println("bool_compare_month = $bool_compare_month")
end

button_start_triggered = signal_connect(button_start, "clicked") do widget
    selected_file_old = get_gtk_property(text_file_old,:text,String)
    selected_file_new = get_gtk_property(text_file_new,:text,String)
    if get_gtk_property(button_start, :label, String) == "Comparing..."
        GAccessor.label(textbox_progress, feedback_already_processing)
    elseif selected_file_old == "" || selected_file_new == ""
        GAccessor.label(textbox_output, feedback_select_both)
        #println("message to select files.")
    elseif !isfile(selected_file_old) || !isfile(selected_file_new)
        GAccessor.label(textbox_output, feedback_both_files)
        #println("message to select actual files.")
    else
        GAccessor.label(button_start, button_start_text_working)
        GAccessor.label(textbox_output, "")
        array_lines_older = Functions.get_file_lines_or_nothing(selected_file_old)
        array_lines_newer = Functions.get_file_lines_or_nothing(selected_file_new)
        if array_lines_older === nothing || array_lines_newer === nothing
            #println("at least one file array is nothing.")
            GAccessor.label(textbox_output, feedback_file_old_blank)
        else
            GAccessor.label(textbox_progress, feedback_checking)

            # send off the work to the module
            result_string = start_comparison(array_lines_older, array_lines_newer, selected_file_old, bool_compare_month)
            
            # output and cleanup
            GAccessor.label(textbox_progress, feedback_completed)
            GAccessor.label(textbox_output, result_string)
            set_gtk_property!(button_start, :label, button_start_text)
        end
    end
end

# fake condition keeps window open unless closed.
if !isinteractive()
    c = Condition()
    signal_connect(window, :destroy) do widget
        notify(c)
    end
    wait(c)
end




