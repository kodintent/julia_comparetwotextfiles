module Functions

export
get_array_filelines_errnothing,
trysave_filestring_truefalse,
get_file_lines_or_nothing,
get_path_child

function get_array_filelines_errnothing(filepath::AbstractString, strip_breaks::Bool, sortlines::Bool, exclude_empty_lines::Bool)

    array_lines_file = tryget_filelines_errnothing(filepath)
    if array_lines_file === nothing return nothing end
    if length(array_lines_file) == 0
        println("    get_array_filelines_errnothing - filepath returned 0 lines.")
        return array_lines_file
    end
    # filter lines if bools require it.
    if strip_breaks || exclude_empty_lines
        array_lines_edited = []
        for line in array_lines_file
            line_keep = line
            line_stripped = strip(line, [' ', '\r', '\n'])
            if strip_breaks line_keep = line_stripped end
            if exclude_empty_lines && line_stripped == "" continue end
            push!(array_lines_edited, line_keep)
        end
        if sortlines sort!(array_lines_edited) end
        return array_lines_edited
    end

    # else return unfiltered lines
    if sortlines sort!(array_lines_file) end
    return array_lines_file
end

function get_file_lines_or_nothing(filepath::AbstractString)
    array_lines = get_array_filelines_errnothing(filepath, true, true, true)
    if array_lines === nothing return nothing end
    #if array_lines == [] return nothing end  # removed, if file is empty so what?
    return array_lines
end



##########################################################

function trysave_filestring_truefalse(filepath_output::AbstractString, content_string::AbstractString)
    try
        open(filepath_output, "w") do io
            write(io, content_string)
        end
    catch
        return false
    end
    return true
end


function tryget_filelines_errnothing(filepath::AbstractString)
    try
        open(filepath, "r") do io
            return readlines(io)
        end
    catch
        return nothing
    end
end

function get_path_child(path::AbstractString)
    parent, child = splitdir(path)
    return child
end


end #module
