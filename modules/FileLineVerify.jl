#=
verifies the presence of lines in a reference file.
breaks up the lines array into 3 levels according to value passed in.
generic form of the code made for comparing journal entry lines quickly.
    
# 20220509 added counts for measuring blank and tooshort lines
# 20200404 added a sort! for the rated lines output
=#

module FileLineVerify
include("./mod_Functions.jl")
using .Functions

export
check_lines_arein_linearray_exact_appendfilename

function check_lines_arein_linearray(array_lines_primary::AbstractArray, array_lines_newer::AbstractArray, selected_file_old::AbstractString, rate_if_inexact::Bool, bool_compare_month::Bool)
    #println("      FLV - check_lines_arein_linearray_m start")
    primary = "source"
    secondary = "merged"
    if rate_if_inexact
        primary = "older"
        secondary = "newer"
    end

    result_string = ""
    array_lines_rated = []
    array_lines_skipped = []

    count_lines_found = 0
    count_lines_tested = 0
    count_lines_exact = 0
    count_lines_testfail = 0
    count_lines_blank = 0
    count_lines_tooshort = 0
    currentbatch_y = ""
    currentbatch_m = ""
    currentbatch_d = ""
    slice_newer_current_y = []
    slice_newer_current_m = []
    slice_newer_current_d = []
    length_array_lines_primary = length(array_lines_primary)
    result_string = string("$result_string Verify result: Found $length_array_lines_primary $primary file lines.")

    #println("      FLV - File line verification line looping begins...")

    for line_primary in array_lines_primary
        
        count_lines_found += 1

        if strip(line_primary) == ""
            count_lines_blank += 1
            continue 
        end # blank line, ignore
        if length(line_primary) < 10
            count_lines_tooshort += 1
            push!(array_lines_skipped, line_primary)
            continue
        end # line is too short to be sliced

        count_lines_tested += 1

        # encase slicing and testing in try catch because of special chars
        try
                    # UPDATE NEWER LINES SLICES

            current_line_y = line_primary[begin:4]
            current_line_m = line_primary[begin:7]
            current_line_d = line_primary[begin:10]
            if current_line_y != currentbatch_y
                currentbatch_y = current_line_y
                slice_newer_current_y = filter(x -> startswith(x, currentbatch_y), array_lines_newer)
            end
            if current_line_m != currentbatch_m
                currentbatch_m = current_line_m;
                slice_newer_current_m = filter(x -> startswith(x, currentbatch_m), slice_newer_current_y)
            end
            if current_line_d != currentbatch_d
                currentbatch_d = current_line_d;
                slice_newer_current_d = filter(x -> startswith(x, currentbatch_d), slice_newer_current_m)
            end
            
            # CHECK IF LINE IS PRESENT

            if line_is_exact_match(line_primary, slice_newer_current_d)
                count_lines_exact += 1
            elseif rate_if_inexact
                if bool_compare_month
                    line_rated = rate_older_line(slice_newer_current_m, line_primary)
                else
                    line_rated = rate_older_line(slice_newer_current_d, line_primary)
                end
                push!(array_lines_rated, line_rated)
            else
                return string("$result_string An older file line was not perfectly matched when exact is required. End comparison.")
            end 
        catch
            count_lines_testfail += 1
            count_lines_tested -= 1
            push!(array_lines_skipped, line_primary)
            result_string = string("$result_string \nLineTestFail: $line_primary.")
            #println("Line test fail: $line_primary")
        end
    end

    # LINE LOOPS COMPLETED
    #println("      FLV - finished line loops.")
    num_lines_not_tested = count_lines_found - count_lines_tested
    num_lines_tested_plus_blank = count_lines_blank + count_lines_tested
    num_lines_skipped = count_lines_found - count_lines_tested
    result_string = string("$result_string Newer Lines Found = $count_lines_found. Exactly matched $count_lines_exact of $count_lines_tested tested. $num_lines_skipped lines skipped including $count_lines_testfail test errors, $count_lines_blank blank lines and $count_lines_tooshort lines <10 chars long. Lines Not Tested = $num_lines_not_tested, Tested+Blank lines = $num_lines_tested_plus_blank.")

    # saving if necessary and reporting

    length_array_lines_rated = length(array_lines_rated)
    if length_array_lines_rated > 0
        #println("length_array_lines_rated = $length_array_lines_rated - save expected")
        #save it
        result_string = string("$result_string Rated $length_array_lines_rated.")
        #println("result_string = $result_string")
        #save the rated lines to a specially named file.
        filepath_rated_lines = string(selected_file_old[begin:end-4], "_rated_lines.txt")
        #println("filepath_rated_lines = $filepath_rated_lines")
        # maybe include some intro lines with file info.
        string_header = "These lines from the newer file, were not exactly matched to lines in the older file.\r\n\r\n"
        sort!(array_lines_rated)
        string_lines_rated = join(array_lines_rated, "\n")
        string_file_content = string(string_header, string_lines_rated)
        saved = Functions.trysave_filestring_truefalse(filepath_rated_lines, string_file_content)
        if saved
            filename = Functions.get_path_child(filepath_rated_lines)
            result_string = string("$result_string Rated lines saved to \"$filename\" in same folder as older file.")
            #println("    Rated lines saved to folder with reference file.")
        else
            result_string = string("$result_string Rated lines created but save to a file failed. Advice: repeat the operation.")
            #println("    Rated lines created but not saved to a file.")
            #verification_successful = false
        end
    else
        if rate_if_inexact result_string = string("$result_string Rated 0.") end
        #println("      FLV - All lines were perfectly matched, no rated lines saved.")
    end

    length_array_lines_skipped = length(array_lines_skipped)
    if length_array_lines_skipped > 0 && selected_file_old != ""
        filepath_skipped_lines = string(selected_file_old[begin:end-4], "_lineverify_skipped_lines.txt")
        string_header = "These lines from the files to compare, were skipped as they were too short, empty or failed verification somehow.\r\n\r\n"
        string_lines_skipped = join(array_lines_skipped, "\n")
        string_file_skipped = string(string_header, string_lines_skipped)
        saved_skipped = Functions.trysave_filestring_truefalse(filepath_skipped_lines, string_file_skipped)
        if saved_skipped
            filename_skipped = Functions.get_path_child(filepath_skipped_lines)
            result_string = string("$result_string Skipped lines saved to \"$filename_skipped\" in same folder as older file.")
        else
            result_string = string("$result_string Skipped lines created but save to a file failed. Advice: repeat the operation.")
        end
    end

    #println("      FLV - final result_string = $result_string")
    return result_string
end

function line_is_exact_match(
    line_primary::AbstractString, slice_newer_current_d::AbstractArray)
    for line_newer in slice_newer_current_d
        if strip(line_newer) == strip(line_primary) return true end
    end
    return false
end

function rate_older_line(
    slice_newer_current_d::AbstractArray, line_primary_to_rate::AbstractString)
    match_rating_max = 0

    for newer_line in slice_newer_current_d
        total_source_word_count = 0
        total_source_word_match_count = 0
        for word_rate in split(line_primary_to_rate, " ") # check if it is in the reference line
            total_source_word_count += 1
            if occursin(word_rate, newer_line) total_source_word_match_count += 1 end
        end
        if total_source_word_match_count > 0 # match count is > 0, calculate the rating and compare
            line_match_rating = floor(100 * total_source_word_match_count / total_source_word_count)
            if match_rating_max < line_match_rating match_rating_max = line_match_rating end
        end
    end

    #get into from float
    match_rating_max_int = Int(floor(Int, match_rating_max))

    if match_rating_max_int > 99 match_rating_max_int = 99 end
    if match_rating_max_int < 10
        line_rated = string("0", match_rating_max_int, "% ", line_primary_to_rate)
    else
        line_rated = string(match_rating_max_int, "% ", line_primary_to_rate)
    end
    return line_rated
end




end # module
