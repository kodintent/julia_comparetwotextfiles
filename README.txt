Important:
- this code cannot be relied on. it is not tested for all use cases.
- there is no claim that this code works in all use cases.
- you should test what you create from this code, for your use case.
- i have tested it for my use cases only, and for that it works.

This project is for a Julia script and its modules:
- help to verify changes to a file, and see differences.
- not the same as available file diff software.
- select 2 versions of text files to compare.
- verify that each source line is exactly present in output file.
- display text report of outcome of the process.
- save to a file, lines that are not exactly matched.

Whats special about this code?
- lines from file not exactly present in other file, are rated and saved to a file.
- rated lines have a % rating to show level of similarity - based on measurement of words present.
- code for rating lines compares the similar lines, analyzes words and calculates a rating of similarity.
- output report includes stats to show verification success.

Script was written to use in Linux.
- minor refactoring to use with windows.
  - in windows use Tk instead of Gtk.

Privacy statement
- this code does not collect or transmit any data.

Freedom statement
- free to use without crediting, obligation or payment.

==== end ====
